#include <canopy/Process.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/io/Selector.h>
#include <canopy/signals/SignalDescriptor.h>

#include <timber/cli/Parameters.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>

#include <integrity/ipc/StandardPipe.h>
#include <iostream>

using namespace ::timber::logging;

int main(int argc, char const** argv)
{

   LogEntry log("run");
   ::timber::cli::Parameters opts;
   auto helpOpt = opts.addParameter(
         ::timber::cli::Flag< bool>("h", "help", "Help message", false, true, false));
   auto timeoutParam = opts.addParameter(
         ::timber::cli::ArgParameter< ::canopy::UInt32>("t", "timeout", "Timeout in seconds (0 to disable)", false,
               "0"));
   auto delayParam = opts.addParameter(
         ::timber::cli::ArgParameter< ::canopy::UInt32>("d", "delay",
               "Wait until input is available on stdin (0 to for infinite delay)", false, "0"));
   auto args = opts.parse(argc, argv);

   if (args.exists(helpOpt)) {
      opts.usage(::std::cerr);
      return 0;
   }

   ::canopy::io::Timeout timeout = ::canopy::io::BLOCKING_TIMEOUT;
   if (args.valueOf(timeoutParam) > 0) {
      timeout = ::canopy::io::Timeout(args.valueOf(timeoutParam) * 1000000000ULL);
      log.info() << "Timeout set to : " << timeout.count() << "ns" << doLog;
   }

   ::canopy::io::Timeout delay = ::canopy::io::BLOCKING_TIMEOUT;
   if (args.valueOf(delayParam) > 0) {
      delay = ::canopy::io::Timeout(args.valueOf(delayParam) * 1000000000ULL);
   }

   try {
      log.info() << "Delay " << delay.count() << "ns or until there is input" << doLog;
      auto result = ::canopy::io::Selector::select(::canopy::io::FileDescriptor::stdin().descriptor(),
            ::canopy::io::IOEvents::READ, delay);
      if (!result.test(::canopy::io::IOEvents::READ)) {
         log.info() << "No data received; exiting" << doLog;
         return 127;
      }
      log.info() << "Starting program" << doLog;
   }
   catch (const ::std::exception& e) {
      log.caught(e) << "Exception while waiting" << doLog;
      return 1;
   }

   ::canopy::Process::Command command;
   command.arguments = args.freeArguments();
   if (command.arguments .empty()) {
      ::std::cerr << "Missing program name" << ::std::endl;
      opts.usage(::std::cerr);
      return 1;
   }
   command.path= ::std::move( command.arguments.front());
   command.arguments.erase( command.arguments.begin());

   ::canopy::Process process;
   ::std::vector< ::canopy::signals::Signal::SignalID> signals;
   signals.push_back(SIGCHLD);
//   signals.push_back(SIGTERM);
//   signals.push_back(SIGINT);
   ::canopy::signals::SignalDescriptor sd = ::canopy::signals::SignalDescriptor::create(signals);

   try {
      process = ::canopy::Process::execute(command);
      log.info() << "Current process id " << ::canopy::Process::currentProcessID() << doLog;
      log.info() << "Child process started : " << process.processID() << doLog;

      ::integrity::ipc::StandardPipe pipe(process);
      ::canopy::io::Selector selector;
      selector.add(sd.descriptor(), ::canopy::io::IOEvents::READ);

      while (pipe.registerEvents(selector)) {
         auto events = selector.select(timeout, ::canopy::io::Selector::EventMap());
         if (events.empty()) {
            log.info() << "Input time-out" << doLog;
            break;
         }
         if (events.find(sd.descriptor()) != events.end()) {
            ::canopy::io::IOEvents sigEvents = events[sd.descriptor()];
            if (sigEvents.test(::canopy::io::IOEvents::READ)) {
               auto sig = sd.read();
               if (sig) {
                  if (sig->id() == SIGCHLD && sig->processID() == process.processID()) {
                     log.info() << "SIGCHLD received from " << sig->processID() << doLog;
                     pipe.shutdownInput();
                  }
                  else {
                     log.warn() << "Unexpected signal " << sig->id() << " received from process " << sig->processID()
                           << doLog;

                  }
               }
            }
         }
         pipe.processEvents(events);
      }
   }
   catch (::std::exception& e) {
      if (process != nullptr) {
         log.caught(e) << "Child process failed : " << process.processID() << doLog;
      }
      else {
         log.caught(e) << "Child process failed to spawn" << doLog;
      }
   }
   if (process == nullptr) {
      return 1;
   }

   if (process.state() != ::canopy::Process::UNBOUND) {
      if (process.state() != ::canopy::Process::RUNNING) {
         log.info() << "Waiting for child to finish" << doLog;
      }
      process.waitFor();
      if (process.state() == ::canopy::Process::TERMINATED) {
         log.info() << "Child process terminated abnormally " << process.processID() << doLog;
         return 1;
      }
      else {
         auto status = process.exitCode();
         log.info() << "Child process finished : " << process.processID() << " with status " << status << doLog;
         return status;
      }
   }
   return 0;
}
