#include <canopy/Process.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/io/Selector.h>
#include <canopy/signals/SignalDescriptor.h>

#include <timber/config/XMLConfiguration.h>
#include <timber/cli/Parameters.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>

#include <map>
#include <iostream>
#include <cstring>

using namespace ::timber::logging;

struct Command
{
      /** The command type */
      enum Type
      {
         LAUNCH, TERMINATE
      };

      virtual ~Command()
      {
      }

      Type type;
      ::std::string id;

      static ::std::unique_ptr< Command> parse(::std::string& xml);

};

struct LaunchCommand : public Command
{
      ::std::string name;
      ::canopy::Process::Command command;
};

struct TerminateCommand : public Command
{
      ::std::string name;
};

/* example command:
<?xml version="1.0" encoding="utf-8" ?>
<command>
  <launch>
    <id>foobar</id>
    <name>ECHO</name>
    <path>/bin/echo</path>
    <arg0>Hello, World</arg0>
  </launch>
</command>
*/

::std::unique_ptr< Command> Command::parse(::std::string& xml)
{
   static const char* END_TAG = "</command>";
   static const auto END_TAG_LENGTH = ::std::strlen(END_TAG);

   LogEntry log("run");

   ::std::unique_ptr< Command> result;

   // find the first instance of "<?xml"
   ::std::string::size_type start = xml.find("<?xml");
   if (start == ::std::string::npos) {
      return result;
   }
   if (start > 0) {
      log.warn() << "Ignoring garbage in the beginning of the stream" << doLog;
      xml.erase(0, start);
      start = 0;
   }
   ::std::string::size_type end = xml.find(END_TAG, start);
   if (end == ::std::string::npos) {
      return result;
   }

   // take the substring
   end += END_TAG_LENGTH;
   ::std::string doctext = xml.substr(start, end - start);
   xml.erase(start, end - start);
   try {
      ::std::istringstream stream(doctext);
      auto config = ::timber::config::XMLConfiguration::readXML(stream);

      if (config->getConfiguration("launch")) {
         auto launchConfig = config->getConfiguration("launch");

         ::std::unique_ptr< LaunchCommand> command(new LaunchCommand());
         command->type = Command::LAUNCH;
         command->id = config->get("id");
         command->name = launchConfig->get("name");
         command->command.path = launchConfig->get("path");
         if (command->command.path.empty()) {
            throw ::std::runtime_error("No path specified");
         }
         for (size_t i = 0, n = launchConfig->count("arg"); i < n; ++i) {
            command->command.arguments.push_back(launchConfig->get("arg[" + ::std::to_string(i) + "]"));
         };

         result = ::std::move(command);
      }
      else if (config->getConfiguration("terminate")) {
         auto terminateConfig = config->getConfiguration("terminate");
         ::std::unique_ptr< TerminateCommand> command(new TerminateCommand());
         command->type = Command::TERMINATE;
         command->id = config->get("id");
         command->name = terminateConfig->get("name");
         result = ::std::move(command);
      }
      else {
         throw ::std::runtime_error("Unexpected command");
      }
   }
   catch (const ::std::exception& e) {
      log.caught(e) << "Failed to parse XML command" << doLog;
   }
   return result;
}

struct Process
{
      LaunchCommand command;
      ::canopy::Process process;

      void terminate()
      {
         if (process.state() == ::canopy::Process::RUNNING) {
            process.terminate();
         }
      }

      void collect()
      {
         LogEntry log("run");
         if (process.state() != ::canopy::Process::UNBOUND) {
            if (process.state() != ::canopy::Process::RUNNING) {
               log.info() << "Waiting for child to finish" << doLog;
            }
            process.waitFor();
            if (process.state() == ::canopy::Process::TERMINATED) {
               log.info() << "Child process terminated abnormally " << process.processID() << doLog;
            }
            else {
               auto status = process.exitCode();
               log.info() << "Child process finished : " << process.processID() << " with status " << status << doLog;
            }
         }
      }

};

static ::canopy::Process launchProcess(const LaunchCommand& cmd)
{
   LogEntry log("run");

   log.info() << "Starting a new program '" << cmd.command.path << "'" << doLog;
   ::canopy::Process process = ::canopy::Process::execute(cmd.command);
   return process;
}

int main(int argc, char const** argv)
{
   typedef ::std::map< ::canopy::Process::ProcessID, Process> ProcessTable;
   typedef ::std::map< ::std::string, Process> ProcessesByName;

   ProcessTable processes;
   ProcessesByName namedProcesses;

   LogEntry log("run");
   ::timber::cli::Parameters opts;
   auto helpOpt = opts.addParameter(
         ::timber::cli::Flag< bool>("h", "help", "Help message", false, true, false));
   auto args = opts.parse(argc, argv);

   if (args.exists(helpOpt)) {
      opts.usage(::std::cerr);
      return 0;
   }

   ::std::vector< ::canopy::signals::Signal::SignalID> signals;
   signals.push_back(SIGCHLD);
//   signals.push_back(SIGTERM);
//   signals.push_back(SIGINT);
   ::canopy::signals::SignalDescriptor sd = ::canopy::signals::SignalDescriptor::create(signals);
   ::canopy::io::FileDescriptor fd = ::canopy::io::FileDescriptor::stdin();

   try {
      ::canopy::io::Timeout timeout = ::canopy::io::BLOCKING_TIMEOUT;

      // read from the stdin
      ::canopy::io::Selector selector;
      selector.add(sd.descriptor(), ::canopy::io::IOEvents::READ);
      selector.add(fd.descriptor(), ::canopy::io::IOEvents::READ);

      ::std::string buffer;

      // wait for instructions
      while (true) {
         auto events = selector.select(timeout, ::canopy::io::Selector::EventMap());

         auto event = events.find(sd.descriptor());
         if (event != events.end()) {
            auto signal = sd.read();
            if (signal != nullptr && signal->id() == SIGCHLD) {
               log.info() << "Child process " << signal->processID() << " has exited" << doLog;
               auto proc = processes.find(signal->processID());
               if (proc == processes.end()) {
                  log.warn() << "Received SIGCHLD from an unknown process " << signal->processID() << doLog;
               }
               else {
                  auto cmd = proc->second;
                  processes.erase(proc);
                  for (auto i = namedProcesses.begin();i!=namedProcesses.end();++i) {
                     if (i->second.process == cmd.process) {
                        namedProcesses.erase(i);
                        break;
                     }
                  }
                  cmd.collect();
               }
            }
         }
         event = events.find(fd.descriptor());
         if (event != events.end()) {
            if (event->second.test(::canopy::io::IOEvents::ERROR)) {
               selector.remove(fd.descriptor(), ::canopy::io::IOEvents::ALL);
               continue;
            }
            char buf[1024];
            auto nRead = fd.read(buf, sizeof(buf));
            if (nRead < 0) {
               selector.remove(fd.descriptor(), ::canopy::io::IOEvents::ALL);
               continue;
            }
            buffer.append(buf, nRead);

            // read any data into the buffer
            for (::std::unique_ptr< Command> command; (command = Command::parse(buffer));) {
               if (command->type == Command::LAUNCH) {
                  Process proc;
                  proc.command = dynamic_cast< LaunchCommand&>(*command);
                  if (namedProcesses.find(proc.command.name) != namedProcesses.end()) {
                     log.warn() << "Child process with name " << proc.command.name << " already exists" << doLog;
                     continue;
                  }
                  proc.process = launchProcess(proc.command);
                  if (proc.process != nullptr) {
                     processes.insert(ProcessTable::value_type(proc.process.processID(), proc));
                     if (!proc.command.name.empty()) {
                        namedProcesses.insert(ProcessesByName::value_type(proc.command.name, proc));
                     }
                     log.info() << "Child process " << proc.process.processID() << " started" << doLog;
                  }
               }
               else if (command->type == Command::TERMINATE) {
                  const TerminateCommand& cmd = dynamic_cast< TerminateCommand&>(*command);
                  auto proc = namedProcesses.find(cmd.name);
                  if (proc == namedProcesses.end()) {
                     log.warn() << "No such named process " << cmd.name << doLog;
                  }
                  else {
                     proc->second.terminate();
                  }
               }
            }
         }
      }
   }
   catch (::std::exception& e) {
      log.caught(e) << "Child process failed to spawn" << doLog;
   }

   // collect any processes that might have finished
   while (!processes.empty()) {
      auto i = processes.begin();
      auto proc = i->second;
      processes.erase(i);
      proc.terminate();
      proc.collect();
   }

   return 0;
}
