/**
 * A simple service.
 */
#include <canopy/Process.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/io/Selector.h>
#include <canopy/io/Pipe.h>
#include <timber/cli/Parameters.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>
#include <iostream>
#include <vector>

using namespace ::timber::logging;
using namespace ::canopy::io;

static bool verbose = false;

struct XPipe
{
      XPipe(::canopy::Process p)
            : _process(p)
      {
         _pipes[0] = Pipe(FileDescriptor::stdin(), p.cinDescriptor());
         _pipes[1] = Pipe(p.coutDescriptor(), FileDescriptor::stdout());
         _pipes[2] = Pipe(p.cerrDescriptor(), FileDescriptor::stderr());
      }
      ::canopy::Process _process;
      Pipe _pipes[3];
};

int main(int argc, char const** argv)
{
   LogEntry log("launch");
   ::timber::cli::Parameters opts;
   auto args = opts.parse(argc, argv);

   ::canopy::Process::Command command;

   command.arguments = args.freeArguments();
   if (command.arguments.empty()) {
      ::std::cerr << "Missing program name" << ::std::endl;
      return 1;
   }

   command.path=command.arguments.front();
   command.arguments.erase(command.arguments.begin());
   ::std::unique_ptr< XPipe> pipe;
   try {
      pipe.reset(new XPipe(::canopy::Process::execute(command)));
      log.info() << "Child process started : " << pipe->_process.processID() << doLog;

      Selector selector;
      ::canopy::io::Timeout timeout(100000000);
      while (true) {
         if (verbose) {
            ::std::cerr << "Entering loop" << ::std::endl;
         }

         for (int i = 0; i < 3; ++i) {
            Pipe& p = pipe->_pipes[i];
            selector.remove(p.writer(), IOEvents::ALL);
            selector.remove(p.reader(), IOEvents::ALL);

            IOEvents revents, wevents;
            p.determineEvents(revents, wevents);
            if (verbose) {
               ::std::cerr << "pipe events " << revents << " & " << wevents << ::std::endl;
            }
            selector.add(p.reader(), revents);
            selector.add(p.writer(), wevents);
            if (revents.empty() && wevents.empty()) {
               if (i == 0) {
                  pipe->_pipes[0].closeWriter();
               }
               else {
                  pipe->_pipes[i].closeReader();
                  pipe->_pipes[0].closeReader();
               }
            }
         }
         if (selector.size() == 0) {
            break;
         }

         auto events = selector.select(timeout, Selector::EventMap());
         for (auto event : events) {
            if (verbose) {
               ::std::cerr << "Processing events" << ::std::endl;
            }
            for (Pipe& p : pipe->_pipes) {
               if (verbose) {
                  ::std::cerr << "Got event " << event.second << ::std::endl;
               }
               if (event.first == p.reader()) {
                  auto x = p.transfer(event.second, IOEvents());
                  if (verbose) {
                     ::std::cerr << "CHECKPOINT 1 " << x << ::std::endl;
                  }
                  break;
               }
               if (event.first == p.writer()) {
                  auto x = p.transfer(IOEvents(), event.second);
                  if (verbose) {
                     ::std::cerr << "CHECKPOINT 2 " << x << ::std::endl;
                  }
                  break;
               }
            }
         }
      }
   }
   catch (::std::exception& e) {
      if (pipe) {
         log.caught(e) << "Child process failed : " << pipe->_process.processID() << doLog;
      }
      else
         log.caught(e) << "Child process failed to spawn" << doLog;
   }

   if (pipe && pipe->_process.state() != ::canopy::Process::UNBOUND) {
      if (pipe->_process.state() != ::canopy::Process::RUNNING) {
         log.info() << "Waiting for child to finish" << doLog;
      }
      pipe->_process.waitFor();
      auto status = pipe->_process.exitCode();
      log.info() << "Child process finished : " << pipe->_process.processID() << " with status " << status << doLog;
      return status;
   }
   return 0;
}
