/**
 * A simple service.
 */
#include <canopy/Process.h>
#include <canopy/fs/FileSystem.h>
#include <canopy/io/FileDescriptor.h>
#include <canopy/io/Selector.h>
#include <canopy/io/Pipe.h>
#include <canopy/net/Socket.h>
#include <canopy/signals/SignalDescriptor.h>
#include <timber/cli/Parameters.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>
#include <iostream>
#include <vector>

using namespace ::timber::logging;
using namespace ::canopy::io;
using namespace ::canopy::net;
using namespace ::canopy::signals;

struct XPipe
{
      XPipe(Socket input, Socket output, ::canopy::Process p)
            : _signalled(false), _input(input), _output(output), _process(p)
      {
         _pipes[0] = Pipe(input.descriptor(), p.cinDescriptor());
         _pipes[1] = Pipe(p.coutDescriptor(), output.descriptor(), 1024);
         _pipes[2] = Pipe(p.cerrDescriptor(), FileDescriptor::stderr());

         LogEntry log("service");
         log.debugging() << "New descriptors " << p.cinDescriptor().descriptor().descriptor() << ","
               << p.coutDescriptor().descriptor().descriptor() << ", " << p.cerrDescriptor().descriptor().descriptor()
               << doLog;

         if (input != output) {
            input.shutdownWrite();
            output.shutdownRead();
         }
      }
      ~XPipe()
      {
         LogEntry log("service");
         try {
            _input.close();
         }
         catch (const ::std::exception& e) {
            log.caught(e) << "Failed to close input socket" << doLog;
         }
         try {
            if (_input != _output) {
               _output.close();
            }
         }
         catch (const ::std::exception& e) {
            log.caught(e) << "Failed to close output socket" << doLog;
         }
         for (int i = 0; i < 3; ++i) {
            try {
               if (i != 2) {
                  _pipes[i].close();
               }
               else {
                  // only close the reader for stderr so that any logs the process might want to write
                  // can still be written.
                  _pipes[i].closeReader();
               }
            }
            catch (const ::std::exception& e) {
               log.caught(e) << "Failed to close pipe " << i << doLog;
            }
         }
         try {
            _process.waitFor();
            log.debugging() << "Process exited with " << _process.exitCode() << doLog;
         }
         catch (const ::std::exception& e) {
            log.caught(e) << "Failed to wait for socket" << doLog;
         }
      }
      bool _signalled;
      Socket _input, _output;
      ::canopy::Process _process;
      Pipe _pipes[3];
};

typedef ::std::unique_ptr< XPipe> XPipePtr;

int main(int argc, char const** argv)
{
   LogEntry log("service");
   Log("service").setLevel(Level::DEBUGGING);
   ::timber::cli::Parameters opts;
   size_t max_pipes = 1;
   auto helpOpt = opts.addParameter(
         ::timber::cli::Flag< bool>("h", "help", "Help message", false, true, false));
   auto maxClientsOpt = opts.addParameter(
         ::timber::cli::ArgParameter< size_t>("m", "maxClients",
               "Maximum number of concurrent clients (0 for unlimited)", false, "1"));
   auto portOpt = opts.addParameter(
         ::timber::cli::ArgParameter< unsigned short>("p", "port", "TCP listening port", true));

   ::timber::cli::ParsedArgs args;
   try {
      args = opts.parse(argc, argv);
   }
   catch (...) {
      opts.usage(::std::cerr);
      return 1;
   }
   if (args.exists(helpOpt)) {
      opts.usage(::std::cerr);
      return 0;
   }

   SignalDescriptor sd = SignalDescriptor::create(SIGCHLD);
   ::canopy::Process::Command command;

   command.arguments = args.freeArguments();
   if ( command.arguments.empty()) {
      ::std::cerr << "Missing program name" << ::std::endl;
      opts.usage(::std::cerr);
      return 1;
   }

   command.path = command.arguments.front();
   command.arguments.erase( command.arguments.begin());
   {
      canopy::fs::FileSystem fs;
      if (!fs.hasPermissions(command.path, canopy::fs::FileSystem::EXECUTE)) {
         ::std::cerr << "Program is not executable: " << command.path << ::std::endl;
         return 1;
      }
   }
   auto ep = Endpoint::createAnyEndpoint(args.valueOf(portOpt), STREAM);
   Selector selector;
   Socket socket = Socket::createBoundSocket(ep);
   log.info() << socket.getBoundAddress().serviceID() << doLog;
   socket.listen(1);
   ::std::vector< XPipePtr> pipes;

   max_pipes = args.valueOf(maxClientsOpt);
   log.debugging() << "Maximum number of clients " << max_pipes << doLog;
   while (true) {
      ::canopy::io::Timeout timeout(10000000000ULL);

      selector.clear();

      // add the rules for monitoring the pipe
      if (!pipes.empty()) {
         for (auto xp = pipes.begin(); xp != pipes.end();) {
            XPipePtr& pipe = *xp;
            size_t selectorSizeBefore = selector.size();
            for (int i = 0; i < 3; ++i) {
               Pipe& p = pipe->_pipes[i];
               IOEvents revents, wevents;
               p.determineEvents(revents, wevents);
               log.debugging() << "pipe events " << i << ": " << revents << " & " << wevents << doLog;
               selector.add(p.reader(), revents);
               selector.add(p.writer(), wevents);

               if (revents.empty() && wevents.empty()) {
                  try {
                     if (i == 0) {
                        log.debugging() << "Closing writer 0 " << doLog;
                        pipe->_pipes[0].closeWriter();
                     }
                     else {
                        log.debugging() << "Closing reader " << i << doLog;
                        pipe->_pipes[i].closeReader();
                        if (pipe->_signalled && i == 1) {
                           try {
                              pipe->_input.shutdownRead();
                           }
                           catch (...) {

                           }
                        }
                     }
                  }
                  catch (const IOException&) {

                  }
               }
            }
            size_t selectorSizeAfter = selector.size();
            if (selectorSizeAfter != selectorSizeBefore) {
               selector.add(sd.descriptor(), IOEvents::READ);
               ++xp;
            }
            else {
               pipe.reset();
               pipes.erase(xp);
            }
         }
      }
      // if there is nothing to select anymore, then accept new connections
      if (max_pipes == 0 || pipes.size() < max_pipes) {
         selector.add(socket.descriptor(), IOEvents::READ);
         if (selector.size() == 1) {
            log.debugging() << "Accepting new connections" << doLog;
            timeout = BLOCKING_TIMEOUT;
         }
      }

      log.debugging() << "Waiting for events" << doLog;
      auto events = selector.select(timeout,Selector::EventMap());
      log.debugging() << "Got " << events.size() << " events" << doLog;

      for (auto event : events) {
         if (event.first == sd.descriptor()) {
            auto sig = sd.read();
            if (sig) {
               log.debugging() << "Got a signal " << sig->id() << doLog;
               for (XPipePtr& pipe : pipes) {
                  if (pipe) {
                     pipe->_signalled = true;
                  }
               }
            }
         }
         else if (event.first == socket.descriptor() && event.second.test(IOEvents::READ)) {
            // we accept on a socket
            try {
               auto client = socket.accept();
               auto child = ::canopy::Process::execute(command);
               XPipePtr pipe(new XPipe(client, client, child));
               log.debugging() << "Accepted a new connection" << doLog;
               pipes.push_back(move(pipe));
            }
            catch (::std::exception& e) {
               log.caught(e) << "Child process failed to spawn" << doLog;
            }
         }
         else {
            for (XPipePtr& pipe : pipes) {
               // handle each of the pipes 3 pipes (stdin,stdout,stderr)
               for (int i = 0; i < 3 && pipe; ++i) {
                  Pipe& p = pipe->_pipes[i];
                  if (event.first == p.reader()) {
                     try {
                        log.debugging() << "Reading from pipe " << i << " " << event.second << doLog;
                        auto x = p.transfer(event.second, IOEvents());
                        if (pipe->_signalled && i == 0) {
                           // keep reading from the socket, but don't try to send it on to the writer
                           p.clear();
                        }
                        log.debugging() << "CHECKPOINT 1 " << x << doLog;
                     }
                     catch (const IOException& e) {
                        log.caught(e) << "Caught IOException" << doLog;
                        try {
                           //    p.close();
                        }
                        catch (...) {
                        }
                     }
                  }
                  if (event.first == p.writer()) {
                     try {
                        log.debugging() << "Writing to pipe " << i << " " << event.second << doLog;
                        auto x = p.transfer(IOEvents(), event.second);
                        log.debugging() << "CHECKPOINT 2 " << x << doLog;
                     }
                     catch (const IOException& e) {
                        log.caught(e) << "Caught IOException" << doLog;
                        try {
                           // p.close();
                        }
                        catch (...) {
                        }
                     }
                  }
               }
            }

         }
      }
   }
   return 0;
}
