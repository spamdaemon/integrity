#ifndef _INTEGRITY_IPC_STANDARDPIPE_H
#define _INTEGRITY_IPC_STANDARDPIPE_H

#ifndef _CANOPY_PROCESS_H
#include <canopy/Process.h>
#endif

#ifndef _CANOPY_IO_PIPE_H
#include <canopy/io/Pipe.h>
#endif

#ifndef _CANOPY_IO_IODESCRIPTOR_H
#include <canopy/io/IODescriptor.h>
#endif

#ifndef _CANOPY_IO_SELECTOR_H
#include <canopy/io/Selector.h>
#endif

namespace integrity {
   namespace ipc {

      /**
       * The process pipe is a bundling of the three standard process channels, stdin, stdout, and stderr. The process
       * can will be controlled by this pipe, by closing the input and outputs as appropriate.
       */
      class StandardPipe
      {
            /** The index of the stdin pipe */
            static constexpr int PIPE_STDIN = 0;
            /** The index of the stdout pipe */
            static constexpr int PIPE_STDOUT = 1;
            /** The index of teh stderr pipe */
            static constexpr int PIPE_STDERR = 2;

            /**
             * Create a new standard pipe between the process and the given descriptors. The provide descriptors
             * will not be closed by this pipe. Only the descriptors associated with the process will be closed.
             * @param process a process
             * @param in a descriptor from which to read input destined for the process (stdin)
             * @param out a descriptor to which output from the process is sent (stdout)
             * @param err a descriptor to whcih error output from the process is sent (stderr)
             */
         public:
            StandardPipe(::canopy::Process process, ::canopy::io::IODescriptor in, ::canopy::io::IODescriptor out,
                  ::canopy::io::IODescriptor err) throws();

            /**
             * Create a new standard pipe tied to the calling process's standard channels.
             * @param process a process
             */
         public:
            StandardPipe(::canopy::Process process) throws();

            /**
             * Destructor
             */
         public:
            ~StandardPipe() throws();

            /**
             * Shutdown the stdin input side of this pipe. This does not actually close
             * the input, but the pipe will not register for events on that descriptor.
             */
         public:
            void shutdownInput();

            /**
             * Shutdown the stdout output side of this pipe. This does not actually close
             * the output, but the pipe will not register for events on that descriptor.
             */
         public:
            void shutdownOutput();

            /**
             * Register events with the specified selector.
             * @param selector a selector
             * @return true if this pipe has registered events with the selector
             */
         public:
            bool registerEvents(::canopy::io::Selector& selector);

            /**
             * Process events for this pipe
             * @param events from a selector
             * @return true if there were any selection events for this pipe
             */
         public:
            bool processEvents(const ::canopy::io::Selector::EventMap& events);

            /** The process to which the pipes are attached */
         private:
            ::canopy::Process _process;

            /** The three pipes, stdin=0, stdout=1,stderr=2 */
         private:
            ::canopy::io::Pipe _pipes[3];

            /** The shutdown status of each pipe */
         private:
            bool _pipeStatus[3];
      };
   }
}

#endif
