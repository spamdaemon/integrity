#include <timber/logging.h>
#include <integrity/ipc/StandardPipe.h>
#include <canopy/io/FileDescriptor.h>

namespace integrity {
   namespace ipc {

      using namespace ::timber::logging;

      StandardPipe::StandardPipe(::canopy::Process process, ::canopy::io::IODescriptor in,
            ::canopy::io::IODescriptor out, ::canopy::io::IODescriptor err) throws ()
            : _process(process), _pipes { ::canopy::io::Pipe(in, process.cinDescriptor()), ::canopy::io::Pipe(
                  process.coutDescriptor(), out), ::canopy::io::Pipe(process.cerrDescriptor(), err) }, _pipeStatus {
                  true, true, true }

      {

      }

      StandardPipe::StandardPipe(::canopy::Process process) throws ()
            : StandardPipe(process, ::canopy::io::FileDescriptor::stdin().descriptor(),
                  ::canopy::io::FileDescriptor::stdout().descriptor(),
                  ::canopy::io::FileDescriptor::stderr().descriptor())

      {

      }

      StandardPipe::~StandardPipe() throws()
      {
         for (int i : { PIPE_STDIN, PIPE_STDOUT, PIPE_STDERR }) {
            try {
               if (i == PIPE_STDIN) {
                  _pipes[i].closeWriter();
               }
               else {
                  _pipes[i].closeReader();
               }
            }
            catch (const ::std::exception& e) {
               ::timber::logging::LogEntry("integrity.ipc.StandardPipe").warn() << "Failed to close file descriptor "
                     << i << " for process " << _process.processID() << ::timber::logging::doLog;
            }
         }
      }

      void StandardPipe::shutdownInput()
      {
         _pipeStatus[PIPE_STDIN] = false;
      }

      void StandardPipe::shutdownOutput()
      {
         _pipeStatus[PIPE_STDOUT] = false;
      }

      bool StandardPipe::registerEvents(::canopy::io::Selector& selector)
      {
         bool registeredEvents = false;

         for (int i : { PIPE_STDIN, PIPE_STDOUT, PIPE_STDERR }) {
            selector.remove(_pipes[i].writer(), ::canopy::io::IOEvents::ALL);
            selector.remove(_pipes[i].reader(), ::canopy::io::IOEvents::ALL);
         }

         // STDIN needs to be handled a little differently from the output descriptors
         {
            ::canopy::io::IOEvents revents, wevents;
            _pipes[PIPE_STDIN].determineEvents(revents, wevents);
            if (!_pipeStatus[PIPE_STDIN]) {
               revents.clearEvents();
            }
            if (revents.empty() && wevents.empty()) {
               _pipes[PIPE_STDIN].closeWriter();
            }
            else {
               selector.add(_pipes[PIPE_STDIN].reader(), revents);
               selector.add(_pipes[PIPE_STDIN].writer(), wevents);
               registeredEvents = true;
            }
         }

         for (int i : { PIPE_STDOUT, PIPE_STDERR }) {
            ::canopy::io::IOEvents revents, wevents;
            _pipes[i].determineEvents(revents, wevents);
            if (!_pipeStatus[i]) {
               wevents.clearEvents();
            }
            if (revents.empty() && wevents.empty()) {
               _pipes[i].closeReader();
            }
            else {
               selector.add(_pipes[i].reader(), revents);
               selector.add(_pipes[i].writer(), wevents);
               registeredEvents = true;
            }
         }

         return registeredEvents;
      }

      bool StandardPipe::processEvents(const ::canopy::io::Selector::EventMap& events)
      {
         bool processedEvents = false;
         for (int i : { PIPE_STDIN, PIPE_STDOUT, PIPE_STDERR }) {
            auto event = events.find(_pipes[i].reader());
            if (event != events.end()) {
               _pipes[i].transfer(event->second, ::canopy::io::IOEvents());
               processedEvents = true;
            }
            event = events.find(_pipes[i].writer());
            if (event != events.end()) {
               if (i == PIPE_STDIN && !_pipeStatus[i]) {
                  _pipes[i].clear();
               }
               else {
                  _pipes[i].transfer(::canopy::io::IOEvents(), event->second);
               }
               processedEvents = true;
            }
         }
         return processedEvents;
      }

   }
}
