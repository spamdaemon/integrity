#ifndef _INTEGRITY_LIFECYCLE_H
#define _INTEGRITY_LIFECYCLE_H

namespace integrity {

   /**
    * The lifecycle of an integrity component.
    */
   enum class Lifecycle
   {
      /**
       * The component is OFF; this is not reported by a component itself,
       * but can be used to represent another components current state.
       */
      OFF,

      /**
       * The component is starting and may be ready to receive messages.
       */
      STARTING,

      /** The component is up and running and ready to receive messages. */
      RUNNING,

      /** The component is shutting down and may no longer be able to receive any messages */
      TERMINATING,

      /**
       *  The component has failed and may no longer be able to receive any messages.
       * If this is a managed component, then it might be possible to restart it.
       */
      FAILED
   };

}
#endif
